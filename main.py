import datetime
from flask import Flask, render_template, request, redirect, Response, url_for
from google.cloud import datastore, storage
import local_constants
from google.auth.transport import requests
import google.oauth2.id_token
import uuid
app = Flask(__name__)
datastore_client = datastore.Client()
firebase_request_adapter = requests.Request()
accepted_extensions = {'docx', 'odt', 'xlsx', 'ods'} 
@app.route('/')
def root():
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    user_info = None
    directory_list = []
    user_storage = None
    shared_files = None
    directory_error_message = None
    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter)
            user_info = retrieveUserInfo(claims)
            directory_error_message = request.args.get('error_message')
            if user_info == None:
                createUserInfo(claims)
                user_info = retrieveUserInfo(claims)
            # get user free storage, shared files and directories   
            user_storage = getFreeStorage(user_info['storage'])
            shared_files = getSharedFiles(user_info['email'])
            directory_list = getUserDirectories(user_info['email'])
           
        except ValueError as exc:  
            error_message = str(exc)
    return render_template('index.html', user_data=claims, error_message=error_message, directory_error_message = directory_error_message, user_info=user_info, directory_list=directory_list, user_storage = user_storage, shared_files = shared_files)

def createUserInfo(claims):
    entity_key = datastore_client.key('UserInfo', claims['email'])
    entity = datastore.Entity(key = entity_key)
    if 'name' in claims:
        name = claims['name']
    else:
        name = 'unknown'
    entity.update({
        'email': claims['email'],
        'name': name,
        'storage': 0
    })
    datastore_client.put(entity)

def retrieveUserInfo(claims):
 entity_key = datastore_client.key('UserInfo', claims['email'])
 entity = datastore_client.get(entity_key)
 return entity

def blobList(prefix):
 storage_client = storage.Client(project=local_constants.PROJECT_NAME)
 return storage_client.list_blobs(local_constants.PROJECT_STORAGE_BUCKET,prefix=prefix)

# add directory as well as a directory entity for the datastore 
def addDirectory(directory_name, userEmail):
    storage_client = storage.Client(project=local_constants.PROJECT_NAME)
    bucket = storage_client.bucket(local_constants.PROJECT_STORAGE_BUCKET)
    if directory_name[len(directory_name)-1] != '/':
        return False
    # generate a key for the directory, add the directrory to the datastore as well as cloud storage
    directoryKey = str(uuid.uuid4())
    blob = bucket.blob(directoryKey+'/')
    directoryEntity = datastore.Entity(key=datastore_client.key('DirectoryInfo', directoryKey))
    directoryEntity.update({
        'name': directory_name,
        'createdBy': userEmail,
        'created': datetime.datetime.now(),
    })
    # upload the directory to the storage and its metadata to the datastore 
    blob.upload_from_string('', content_type='application/x-www-formurlencoded;charset=UTF-8')

    datastore_client.put(directoryEntity)       


# add file as well as a file entity for the datastore 
def addFile(file, userEmail, directoryId, parentFile, version):
    storage_client = storage.Client(project=local_constants.PROJECT_NAME)
    bucket = storage_client.bucket(local_constants.PROJECT_STORAGE_BUCKET)
    # generate a unique identifier for the file using the storage bucket
    file_key = str(uuid.uuid4())
    fileSize = len(file.getbuffer())
    if directoryId is None:
        blob = bucket.blob(file_key)
        blob.upload_from_file(file)
    else:
        blob = bucket.blob(directoryId + '/' + file_key)
        blob.upload_from_file(file)
    

    # create an entity in the datastore for the file with the same key
    fileEntity = datastore.Entity(key=datastore_client.key('FileInfo', file_key))
    fileEntity.update({
        'name': file.filename,
        'url': blob.public_url,
        'uploadedBy': userEmail,
        'created': datetime.datetime.now(),
        'sharedWith': [],
        'directoryKey': directoryId,
        'parentFile': parentFile,
        'version': version
    })
    # save the entity in the datastore
    datastore_client.put(fileEntity)
    updateStorageSize(userEmail)
def downloadBlob(fileRoute, fileName):
 storage_client = storage.Client(project=local_constants.PROJECT_NAME)
 bucket = storage_client.bucket(local_constants.PROJECT_STORAGE_BUCKET)
 blob = bucket.blob(fileRoute)
 file_bytes = blob.download_as_bytes()
 # this is to ensure that when a user downloads a file its name is set to the actual name and not the key 
 headers = {
    'Content-Disposition': f'attachment; filename="{fileName}"'
 }
 return Response(file_bytes, headers=headers, mimetype='application/octet-stream')

@app.route('/directory/<string:directoryId>')
def view_directory(directoryId):
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    times = None
    user_info = None
    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter)
            isDirectoryEmpty = False
            currentDirectory = directoryId
            directory = getDirectoryById(directoryId)
            files = getFilesInDirectory(directoryId)
            if len(files) == 0:
                isDirectoryEmpty = True
            error_message = request.args.get('error_message') 
        except ValueError as exc:
            error_message = str(exc)        
    return render_template('directory.html', directory=directory, files=files, currentDirectory=currentDirectory, isDirectoryEmpty = isDirectoryEmpty, error_message = error_message)


def getDirectoryById(directoryId):
    directory_key = datastore_client.key('DirectoryInfo', directoryId)
    directory = datastore_client.get(directory_key)
    if not directory:
        raise Exception(f"No directory with id {directoryId} found")
    return directory

def getFilesInDirectory(directoryKey):
    query = datastore_client.query(kind='FileInfo')
    query.add_filter('directoryKey', '=', directoryKey)
    query.add_filter('version', '=', 1)
    query.order = ["name"]
    file_entities = list(query.fetch())
    return file_entities

@app.route('/download_file/<string:fileId>', methods=['POST'])
def downloadFile(fileId):
 id_token = request.cookies.get("token")
 fileRoute = None 
 fileName = None 
 if id_token:
    try:
        claims = google.oauth2.id_token.verify_firebase_token(id_token,firebase_request_adapter)
        file_key = datastore_client.key('FileInfo', fileId)
        file_entity = datastore_client.get(file_key)
        directoryKey = file_entity['directoryKey']
        fileName = file_entity['name']
        fileRoute = directoryKey + '/' + fileId
        return downloadBlob(fileRoute, fileName)
    except ValueError as exc:
        error_message = str(exc)
 return redirect('/')

@app.route('/add_directory', methods=['POST'])
def addDirectoryHandler():
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    times = None
    user_info = None
    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter)
            directory_name = request.form['dir_name']
            if directory_name == '' or directory_name[len(directory_name) - 1] != '/':
                return redirect('/')
            user_info = retrieveUserInfo(claims)
            existing_dirs = getUserDirectories(claims['email'])
            existing_dir_names = [d['name'] for d in existing_dirs]
            if directory_name in existing_dir_names:
                # redirect to homepage with an error message
                error_message = "You have already created a directory with this name."
                return redirect(url_for('root', error_message=error_message))
            else:
                # add new directory
                addDirectory(directory_name, claims['email'])
        except ValueError as exc:
            error_message = str(exc)
    return redirect('/')

def getUserDirectories(userEmail): 
    query = datastore_client.query(kind = "DirectoryInfo")
    query.add_filter("createdBy", "=", userEmail)
    directories = list(query.fetch())
    return directories
@app.route('/upload_file/<string:directoryId>', methods=['post'])
def uploadFileToDirectoryHandler(directoryId):
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter)
            file = request.files['file_name']
            if file.filename == '' or acceptedFile(file.filename)==False:
                error_message = "The file extension is not accepted"
                return redirect(url_for('view_directory', directoryId=directoryId, error_message=error_message))
            user_info = retrieveUserInfo(claims)
            userKey = datastore_client.key('UserInfo', claims['email'])
            userEntity = datastore_client.get(userKey)
            currentStorageSize = userEntity['storage']
            fileSize = len(file.getbuffer())
            # check if uploading the file will exceed the storage limit
            if currentStorageSize + fileSize >= 5 * 1024 * 1024:  # 5MB limit
                error_message =  str("You have exceeded your storage limit of 5MB. Please delete some files before uploading more.")
            else:     
                addFile(file, claims['email'], directoryId, None, 1)
        except ValueError as exc:
            error_message = str(exc)
    return redirect(url_for('view_directory', directoryId=directoryId, error_message=error_message))

@app.route('/deleteFromDirectory/<string:fileId>/<string:directoryId>', methods=['post'])
def deleteFileHandler(fileId, directoryId):
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter)
            # Delete from cloud storage 
            bucket_name = local_constants.PROJECT_STORAGE_BUCKET
            storage_client = storage.Client(project=local_constants.PROJECT_NAME)
            bucket = storage_client.get_bucket(bucket_name)
            # Delete from datastore 
            key = datastore_client.key("FileInfo", fileId)
            entity = datastore_client.get(key)
            parentFile = entity['parentFile']
            datastore_client.delete(key)
            blob_name = directoryId + '/' + fileId
            blob = bucket.get_blob(blob_name)
            blob.delete()
            updateStorageSize(claims['email'])
            # if file is a version redirect to versions 
            if parentFile:
                return redirect(url_for('versions', fileId = parentFile))
            # if parent file is None it means we need to delete all versions of this file 
            else: 
                # Delete all versions of the file
                query = datastore_client.query(kind='FileInfo')
                query.add_filter('parentFile', '=', fileId)
                files = list(query.fetch())
                for file in files:
                    key = datastore_client.key("FileInfo", file.key.id_or_name)
                    entity = datastore_client.get(key)
                    blob_name = directoryId + '/' + file.key.id_or_name
                    blob = bucket.get_blob(blob_name)
                    blob.delete()
                    datastore_client.delete(key)
                    updateStorageSize(claims['email'])
                return redirect(url_for('view_directory', directoryId=directoryId, error_message = error_message))
        except ValueError as exc:
            error_message = str(exc)
        return redirect('/')    

@app.route('/deleteDirectory/<string:directoryId>', methods=['post'])
def deleteDirectory(directoryId):
    bucket_name = local_constants.PROJECT_STORAGE_BUCKET
    blob_name = directoryId + '/'
    storage_client = storage.Client(project=local_constants.PROJECT_NAME)
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(blob_name)
    blob.delete()
    key = datastore_client.key("DirectoryInfo", directoryId)
    datastore_client.delete(key)
    return redirect ('/')

@app.route('/versions/<fileId>')
def versions(fileId):
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    isUserOwner = False
    if id_token:
        try: 
            claims = google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter)
            file_key = datastore_client.key('FileInfo', fileId)
            file_entity = datastore_client.get(file_key)
            # Retrieve all versions of the file
            query = datastore_client.query(kind='FileInfo')
            query.add_filter('parentFile', '=', fileId)
            query.order = ['created']
            version_entities = list(query.fetch())
            if file_entity['uploadedBy'] == claims['email']:
                isUserOwner = True
            error_message = request.args.get('error_message')
        except ValueError as exc:
            error_message = str(exc)
    # Render the versions template with the versions data
    return render_template('versions.html', file=file_entity, versions=version_entities, error_message = error_message, isUserOwner=isUserOwner)

@app.route('/upload_version/<fileId>', methods=['post'])
def uploadNewVersionHandler(fileId):
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter)
            file = request.files['file_name']
            if file.filename == '' or acceptedFile(file.filename)==False:
                return redirect(url_for('versions', fileId = fileId))
            # get the directory 
            file_key = datastore_client.key('FileInfo', fileId)
            file_entity = datastore_client.get(file_key)
            directoryId = file_entity['directoryKey']
            # query the datastore for all file versions with the same parent file ID
            query = datastore_client.query(kind='FileInfo')
            query.add_filter('parentFile', '=', fileId)
            files = list(query.fetch())
            # if there are files whose parent file is the one we're looking for we will set the latest version 
            # otherwise there is only one version hence latest version = 1
            if(files):
                latestVersion = max(file['version'] for file in files)
            else:
                latestVersion = 1
            userKey = datastore_client.key('UserInfo', claims['email'])
            userEntity = datastore_client.get(userKey)
            currentStorageSize = userEntity['storage']
            fileSize = len(file.getbuffer())
            # Check if uploading the file will exceed the storage limit
            if currentStorageSize + fileSize >= 5 * 1024 * 1024:  # 5MB limit
                error_message =  str("You have exceeded your storage limit of 5MB. Please delete some files before uploading more.")
            else:     
                addFile(file, claims['email'], directoryId, fileId, latestVersion+1)
            
        except ValueError as exc:
            error_message = str(exc)
        return redirect(url_for('versions', fileId = fileId, error_message=error_message))
def acceptedFile(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in accepted_extensions

@app.route('/shareFile/<fileId>/<directoryId>', methods=['POST'])
def share_file(fileId, directoryId):
    id_token = request.cookies.get("token")
    error_message = None
    claims = None
    user_info = None
    file_entity = None
    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(id_token, firebase_request_adapter)
            user_info = retrieveUserInfo(claims)
            # Retrieve the file entity from datastore
            file_key = datastore_client.key('FileInfo', fileId)
            file_entity = datastore_client.get(file_key)
            # Get the email address entered by the user
            shared_email = request.form.get('shared_email')
            if not shared_email:
                error_message = "Please enter an email address to share with."
                return redirect(url_for('view_directory', directoryId=directoryId, error_message = error_message))
            # Check if the email address belongs to a registered user
            shared_user_info = retrieveUserInfo({'email': shared_email})
            if shared_user_info is None:
                error_message = "No user found with email " + shared_email
            else:
                # Add the shared email to the file entity's sharedWith array
                shared_with = file_entity.get('sharedWith', [])
                shared_with.append(shared_email)
                file_entity['sharedWith'] = shared_with
                datastore_client.put(file_entity)
        except ValueError as exc:
            error_message = str(exc)
    # redirect back to the directory page
    return redirect(url_for('view_directory', directoryId=directoryId, error_message = error_message))

#function to get free storage for the user and display in friendly format 
def getFreeStorage(storage):
    free_storage = (5 * 1024 * 1024) - storage
    if free_storage < 0:
        return '0 Bytes'
    elif free_storage < 1024:
        return str(free_storage) + ' Bytes'
    elif free_storage < 1024 * 1024:
        return str(round(free_storage / 1024, 2)) + ' KB'
    else:
        return str(round(free_storage / (1024 * 1024), 2)) + ' MB'

# get all the files uploaded by the user and add up their sizes  
def updateStorageSize(userEmail):
    bucket_name = local_constants.PROJECT_STORAGE_BUCKET
    storage_client = storage.Client(project=local_constants.PROJECT_NAME)
    bucket = storage_client.get_bucket(bucket_name)
    query = datastore_client.query(kind='FileInfo')
    query.add_filter('uploadedBy', '=', userEmail)
    results = query.fetch()
    total_size = 0
    for file_info in results:
        blob_name = file_info['directoryKey'] + '/' + file_info.key.id_or_name
        blob = bucket.get_blob(blob_name)
        total_size += blob.size
    userKey = datastore_client.key('UserInfo', userEmail)
    userEntity = datastore_client.get(userKey)
    userEntity['storage'] = total_size
    datastore_client.put(userEntity)
def getSharedFiles(userEmail):
    # Query the datastore 
    query = datastore_client.query(kind="FileInfo")
    # add a filter to the query to find file entities where the userEmail is in the sharedWith array.
    query.add_filter('sharedWith', '=', userEmail)
    # get the list of files that were shared with the user
    shared_files = list(query.fetch())
    
    return shared_files
if __name__ == '__main__':
 app.run(host='127.0.0.1', port=8080, debug=True)
 